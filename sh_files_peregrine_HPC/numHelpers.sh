#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=numH
#SBATCH --output=statsRawDATA/numberOfHelpersPerDominant/model_selection/numH.out 
#SBATCH --mem=30G

module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/numberOfHelpersPerDominant/model_selection/numH.R
