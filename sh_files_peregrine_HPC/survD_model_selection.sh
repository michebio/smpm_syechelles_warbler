#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=survDomOnly
#SBATCH --output=statsRawDATA/survival/model_selection/surv_DomOnly.out
#SBATCH --mem=100G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/survival/model_selection/surv_DomOnly.R
