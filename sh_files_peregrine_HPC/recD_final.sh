#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=recD
#SBATCH --output=statsRawDATA/reproductionDominant/final_model/recDom_final.out 
#SBATCH --mem=80G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/reproductionDominant/final_model/recDom_final.R
