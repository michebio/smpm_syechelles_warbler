#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=surv_subordinates
#SBATCH --output=statsRawDATA/survival/subordinates/model_selection/surv_sub.out 
#SBATCH --mem=20G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/survival/subordinates/model_selection/surv_sub.R 
