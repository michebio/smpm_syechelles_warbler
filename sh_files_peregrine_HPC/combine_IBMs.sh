#!/bin/bash
#SBATCH --time=3:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=2
#SBATCH --partition=gelifes
#SBATCH --job-name=run
#SBATCH --output=population_model/IBM/2_combineIBMs_%A_%a.out
#SBATCH --mem=70G


module load R/4.0.0-foss-2020a 
Rscript population_model/IBM/2_combineIBMs.R &
Rscript population_model/IBM/2_combineIBMs_2.R &
wait
