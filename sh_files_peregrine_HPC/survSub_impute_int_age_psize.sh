#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=surv_sub_intAge_impute
#SBATCH --output=statsRawDATA/survival/subordinates/model_imputation/psize_interaction_with_age/output_hpc.out 
#SBATCH --mem=100G

 
module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/survival/subordinates/model_imputation/psize_interaction_with_age/impute_st.R






