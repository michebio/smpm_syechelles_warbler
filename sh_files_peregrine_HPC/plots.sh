#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=plotsSMPM
#SBATCH --output=population_model/stocModelOutput/4_violinplot.out 
#SBATCH --mem=20G


module load V8/3.2.0-foss-2020a-R-4.0.0
module load R/4.0.0-foss-2020a 
Rscript population_model/stocModelOutput/4_violinplot.R 
