#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=run
#SBATCH --output=population_model/IBM/4_plots.out
#SBATCH --mem=40G


module load R/4.0.0-foss-2020a 
Rscript population_model/IBM/4_plotNew.R
