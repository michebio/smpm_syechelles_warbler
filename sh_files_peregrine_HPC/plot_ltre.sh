#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=plot
#SBATCH --output=population_model/stocModelOutput/LTRE/output_ltre/plt.out
#SBATCH --mem=10G


 
module load R/4.0.0-foss-2020a 
Rscript population_model/stocModelOutput/LTRE/test_plot_ltre.R






