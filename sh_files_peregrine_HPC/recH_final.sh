#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=recH_F
#SBATCH --output=statsRawDATA/reproductionHelper/final_model/recH_final.out 
#SBATCH --mem=15G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/reproductionHelper/final_model/rh1.R 
