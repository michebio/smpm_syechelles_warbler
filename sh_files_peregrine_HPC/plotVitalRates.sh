#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=plot
#SBATCH --output=statsRawDATA/plotVitalRates/plotVitalRates.out 
#SBATCH --mem=80G


module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/plotVitalRates/plotVitalRates.R
