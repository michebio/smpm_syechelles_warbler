#!/bin/bash
#SBATCH --time=2-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=run
#SBATCH --output=population_model/stocModelOutput/elasticity/Elas_pert.out
#SBATCH --mem=30G



module load R/4.0.0-foss-2020a 
Rscript population_model/stocModelOutput/elasticity/Elas_pert.R
