#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=ent
#SBATCH --output=statsRawDATA/enteringAsH/model_selection/ent_model_selection.out 
#SBATCH --mem=20G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/enteringAsH/model_selection/ent.R 
