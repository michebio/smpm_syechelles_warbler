#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=recD
#SBATCH --output=statsRawDATA/reproductionDominant/model_comparison/rec_DomOnly.out
#SBATCH --mem=70G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/reproductionDominant/model_comparison/recD.R
