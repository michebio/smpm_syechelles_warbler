#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=numh
#SBATCH --output=statsRawDATA/numberOfHelpersPerDominant/final_model/numH_final.out
#SBATCH --mem=60G

module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/numberOfHelpersPerDominant/final_model/numH_final.R
