#!/bin/bash
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=gelifes
#SBATCH --job-name=ent
#SBATCH --output=statsRawDATA/enteringAsH/final_model_age/ent_final.out 
#SBATCH --mem=10G

module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/enteringAsH/final_model_age/ent_final.R
