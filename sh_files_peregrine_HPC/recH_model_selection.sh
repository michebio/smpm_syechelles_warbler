#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=recH
#SBATCH --output=statsRawDATA/reproductionHelper/model_selection/recH_model_selection.out 
#SBATCH --mem=10G

module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/reproductionHelper/model_selection/rh.R 
