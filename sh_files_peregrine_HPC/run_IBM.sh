#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --array=1-1000
#SBATCH --partition=gelifes
#SBATCH --job-name=run
#SBATCH --output=population_model/IBM/file_out/1_runIBM_%a.out
#SBATCH --mem=2G


module load R/4.0.0-foss-2020a 
R --vanilla  --no-save --args ${SLURM_ARRAY_TASK_ID} <  population_model/IBM/1_runIBM.R
