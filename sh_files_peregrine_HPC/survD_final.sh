#!/bin/bash
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=gelifes
#SBATCH --job-name=survD
#SBATCH --output=statsRawDATA/survival/final_model/surv_DomOnly_final.out
#SBATCH --mem=15G



module load R/4.0.0-foss-2020a 
Rscript statsRawDATA/survival/final_model/surv_DomOnly_final.R
