Scripts and dataset used to calculate the results presented in the manuscript:

**Population-level consequences of facultatively cooperative behaviour in a stochastic environment**

written by Michela Busana, Dylan Childs, Terry Burke, Jan Komdeur, David Richardson and Hannah Dugdale. [doi:10.1111/1365-2656.13618](https://doi.org/10.1111/1365-2656.13618)

Code is written and maintained by Michela Busana. It is archived at [doi:10.34894/UWUGZH](https://doi.org/10.34894/UWUGZH)

# Language and required packages

Code is written in [R (v. 4.4.0)](https://cran.r-project.org/) and [Stan (v. 2.19.0)](https://mc-stan.org/).

Necessary R are packages are:

- [rethinking (v. 2.13)](https://github.com/rmcelreath/rethinking)
- [cmdstanr (v. 0.1.3)](https://mc-stan.org/cmdstanr/)
- [data.table (v. 1.14.0)](https://cran.r-project.org/web/packages/data.table/data.table.pdf)

Additional R packages used are:

- bayesplot
- Cairo
- doBy
- dqrng
- export
- fftw
- ggplot2
- MASS
- matlib
- matrixcalc
- mcmcse
- MESS
- mgcv
- mice
- plotrix
- posterior
- randomForest
- RcppAlgos
- readxl
- reldist
- rgl
- rlist
- Rmisc
- scales
- stringr
- tidyverse
- vioplot
- viridis
- withr

# List of sub-folders

Code is organized in multiple sub-folders. 
A file typically starts with a brief explanation of its use.
Please, start the project by opening the `smpm_syechelles_warbler.Rproj` file in [RStudio](https://support.rstudio.com/hc/en-us/articles/200526207-Using-Projects) to automatically set reproducible relative paths to files.

The list of folders is:

- **statsRawDATA**: contains data on female Seychelles warblers (`data_year_2019.txt`) and script to analyze their vital rates in a Bayesian framework (see method section of the manuscript). Results of the statistical analyses are used to parameterize a stochastic matrix population model (SMPM). The folder is divided into additional sub-folders:

  - **survival**: contains script to analyze survival probabilities of dominants and subordinates (helpers + non-helpers)
  - **reproductionDominant**: contains scripts to analyze the reproduction probabilities of dominants
  - **reproductionHelper**: contains scripts to analyze the reproduction probabilities of helpers (note that non-helpers do not reproduce)
  - **enteringAsH**: contains scripts to analyze the probability of an offspring becoming a helper or non-helper
  - **transitionToDominant**: contains scripts to calculate the probability to transition from a helper/non-helper status to a dominant status
  - **numberOfHelpersPerDominant**: contains scripts to estimate the probability that a dominant receives help
  - **plotVitalRates**: contains scripts to plot the vital rates (reproducing Fig. 3)

- **population_model** divided in:

  - **stocModelOutput**: contains scripts to run a stochastic matrix population model (SMPM) and run perturbation analyses
  - **IBM**: contains scripts to run an individual based-model (IBM) to validate the SMPM and test for uncertainty in parameter estimates.

- **sh_files_peregrine_HPC**: contains bash scripts used on the [HPC cluster of the University of Groningen](https://www.rug.nl/society-business/centre-for-information-technology/research/services/hpc/facilities/peregrine-hpc-cluster?lang=en). Bash scripts give an indication of the requirements (CPUs, memory and time) to run the analyses on an HPC cluster, but should be modified to be used on your HPC of choice. To run the scripts on a personal computer, we recommend only running a short test by setting `test <- TRUE` at the top of R scripts (whenever appropriate). When `test <- FALSE`, the scripts need extensive computing resources, and we recommend taking advantage of an HPC cluster.

# License

Copyright 2021 Michela Busana and the University of Groningen

The code in this repository is licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


