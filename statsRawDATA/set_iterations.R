#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Specify warm-up (wm), number of iterations (it), number of cores (cor_n) and  
# number of chains (chain_n) to run Bayesian models 
# for testing (test == TRUE, models do NOT converge)
# or to run the model to convergency (test == FALSE)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if(test == FALSE){
  wm <- 30000
  it <- 60000
  cor_n <- 4
  chain_n <- 4
} else if(test == TRUE){
  wm <- 5
  it <- 100
  cor_n <- 2
  chain_n <- 2
} else {
  stop("Error: please set test equal to TRUE or FALSE")
}
