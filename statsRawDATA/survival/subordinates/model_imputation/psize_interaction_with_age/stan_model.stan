functions{


    vector merge_missing( int[] miss_indexes , vector x_obs , vector x_miss ) {
        int N = dims(x_obs)[1];
        int N_miss = dims(x_miss)[1];
        vector[N] merged;
        merged = x_obs;
        for ( i in 1:N_miss )
            merged[ miss_indexes[i] ] = x_miss[i];
        return merged;
    }
}
data{
  int N;
  int N_seas;
  int Surv[N];
  int missing[N];
  int missing_psize;
  vector[N] age;
  int st[N];
  int seas[N];
  vector[N] Psize_c;
  int Psize_c_missidx[missing_psize];
}
parameters{
  real a_int;
  vector[N_seas] cont_seas;
  real<lower=0> sigma_seas;
  real aST;
  real bA;
  real bAB_A1;
  real<lower=0,upper=1> k;
  real bP;
  real nu_ps;
  real<lower=0> sigma_ps;
  vector[missing_psize] Psize_c_impute;
}
model{
  vector[N] p;
  vector[N] Psize_c_merge;
  sigma_ps ~ exponential( 1 );
  nu_ps ~ normal( 0 , 0.5 );
  Psize_c_merge = merge_missing(Psize_c_missidx, to_vector(Psize_c), Psize_c_impute);
  Psize_c_merge ~ normal( nu_ps , sigma_ps );
  bP ~ normal( 0 , 5 );
  k ~ beta( 2 , 2 );
  for ( i in 1:N ) 
      if ( missing[i] == 0 ) st[i] ~ bernoulli( k );
  bA ~ normal( 0 , 5 );
  bAB_A1 ~ normal( 0 , 5 );
  aST ~ normal( 0 , 5 );
  sigma_seas ~ exponential( 1 );
  cont_seas ~ normal( 0 , 10 );
  a_int ~ normal( 0 , 5 );
  cont_seas ~ normal( a_int , sigma_seas );
  for ( i in 1:N ) // use the k to update the surv prob when there are missing data
      if ( missing[i] == 1 ) target += log_sum_exp(
        log(k) + bernoulli_logit_lpmf(Surv[i] | inv_logit(a_int + aST + (bAB_A1 + bA) * age[i]) + bP * Psize_c_merge[i]), // Survival Prob when the status st = 1 
        log(1 - k) +	bernoulli_logit_lpmf(Surv[i] | inv_logit(a_int + bA * age[i]) + bP * Psize_c_merge[i]) // Survival Prob when the Status st = 0 
        ); 
          // the log_sum_exp function calculates 
  for ( i in 1:N ) {
    p[i] = cont_seas[seas[i]] + aST * st[i] + (bAB_A1 * st[i] + bA) * age[i] + bP * Psize_c_merge[i];
    p[i] = inv_logit(p[i]);
  }
  for ( i in 1:N ) 
      if ( missing[i] == 0 ) Surv[i] ~ bernoulli( p[i] ); // equivalent
}
generated quantities{
    vector[N] PrAB;
    vector[N] lpAB;
    vector[N] lpH;
    vector[N] Psize_c_merge;
    Psize_c_merge = merge_missing(Psize_c_missidx, to_vector(Psize_c), Psize_c_impute);
    for ( i in 1:N ) {
        lpH[i] = log(1 - k) + bernoulli_logit_lpmf(Surv[i] | exp(a_int + bA * age[i]));
    }
    for ( i in 1:N ) {
        lpAB[i] = log(k) + bernoulli_logit_lpmf(Surv[i] | exp(a_int + aST + (bAB_A1 + bA) * age[i]));
    }
    for ( i in 1:N ) {
        PrAB[i] = exp(lpAB[i])/(exp(lpAB[i]) + exp(lpH[i]));
    }
}
