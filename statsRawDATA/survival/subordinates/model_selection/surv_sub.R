#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Script to test and compare the fit  of multilevel binomial models
# for the survival of helpers and non-helpers.
# Explanatory variables considered are status (helper or non-helper),
# age, age squared, standardised population size
# From this script I exclude all the missing data on the status.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rm(list=ls())
library(rethinking)
library(mice)
#location to save outputs
path_loc <- "statsRawDATA/survival/subordinates/model_selection/"

set.seed(999)

test <- TRUE
#test <- FALSE
source("statsRawDATA/set_iterations.R")

# read data file
d <- read.table("statsRawDATA/data_year_2019.txt")
d <- d[!is.na(d$Surv) & d$Status!="BrF",c("Surv", "SeasonID", "Psize_c", "age", "age2", "age3", "Status", "totD")]
d$missing <- ifelse(d$Status=="NSA", 1, 0)
d<- d[d$Status!="NSA",]
d[d$Status=="H", "st"] <- 0
d[d$Status=="AB", "st"] <- 1
#d[d$Status=="NSA", "st"] <- NA
d$seas <- coerce_index(as.factor(d$SeasonID))

check_index(d$seas)




s1 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas  + aST*st + (bAB_A1*st+bA)*age + (bAB_A2*st+bA2)*age2 + (bAB_P*st+bP)*Psize_c,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    aST ~ dnorm(0,5),
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5),
    bA2 ~ dnorm(0,5),
    bAB_A2 ~ dnorm(0,5),
    bP ~ dnorm(0,5),
    bAB_P ~ dnorm(0,5),
    Psize_c ~ dnorm(nu_ps, sigma_ps),
    nu_ps ~ dnorm(0, 0.5),
    sigma_ps ~ dexp(1)
  ),
  data=d[,c("Surv", "seas", "age","age2","Psize_c","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))


s2 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas  + aST*st + (bAB_A1*st+bA)*age + (bAB_A2*st+bA2)*age2 + bP*Psize_c,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    aST ~ dnorm(0,5),
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5),
    bA2 ~ dnorm(0,5),
    bAB_A2 ~ dnorm(0,5),
    bP ~ dnorm(0,5),
    Psize_c ~ dnorm(nu_ps, sigma_ps),
    nu_ps ~ dnorm(0, 0.5),
    sigma_ps ~ dexp(1)
  ),
  data=d[,c("Surv", "seas", "age","age2","Psize_c","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))


s3 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas  + aST*st + (bAB_A1*st+bA)*age + bP*Psize_c,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    aST ~ dnorm(0,5),
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5),
    bP ~ dnorm(0,5),
    Psize_c ~ dnorm(nu_ps, sigma_ps),
    nu_ps ~ dnorm(0, 0.5),
    sigma_ps ~ dexp(1)
  ),
  data=d[,c("Surv", "seas", "age","Psize_c","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))


s4 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas + bAB_A1*st + bA*age + bA2*age2 + bP*Psize_c,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5),
    bA2 ~ dnorm(0,5),
    bP ~ dnorm(0,5),
    Psize_c ~ dnorm(nu_ps, sigma_ps),
    nu_ps ~ dnorm(0, 0.5),
    sigma_ps ~ dexp(1)
  ),
  data=d[,c("Surv", "seas", "age","age2","Psize_c","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))


s5 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas + aST*st + bA*age + bP*Psize_c,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    bA ~ dnorm(0,5),
    aST ~ dnorm(0,5),
    bP ~ dnorm(0,5),
    Psize_c ~ dnorm(nu_ps, sigma_ps),
    nu_ps ~ dnorm(0, 0.5),
    sigma_ps ~ dexp(1)
  ),
  data=d[,c("Surv", "seas", "age","Psize_c","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))


s6 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas + bAB_A1*st + bA*age,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5)
  ),
  data=d[,c("Surv", "seas", "age","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))


s7 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas + bA*age,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    bA ~ dnorm(0,5)
  ),
  data=d[,c("Surv", "seas", "age")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))

s8 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas + bAB_A1*st,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    bAB_A1 ~ dnorm(0,5)
  ),
  data=d[,c("Surv", "seas", "st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))

s9 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas  + aST*st + (bAB_A1*st+bA)*age,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    aST ~ dnorm(0,5),
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5)
  ),
  data=d[,c("Surv", "seas", "age","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))

s10 <- ulam(
  alist(
    Surv ~ dbinom(1, p),
    logit(p) <- a_int + cont_seas[seas]*sigma_seas  + aST*st + (bAB_A1*st+bA)*age + (bAB_A2*st+bA2)*age2,
    
    a_int ~ dnorm(0, 5),
    cont_seas[seas] ~ dnorm(0, 10),
    sigma_seas ~ dexp(1),
    
    aST ~ dnorm(0,5),
    bA ~ dnorm(0,5),
    bAB_A1 ~ dnorm(0,5),
    bA2 ~ dnorm(0,5),
    bAB_A2 ~ dnorm(0,5)
  ),
  data=d[,c("Surv", "seas", "age","age2","st")],
  warmup=wm, iter=it, cores=cor_n, chains=chain_n,
  log_lik = T, cmdstan=TRUE, control=list(adapt_delta=0.95))






sink(paste0(path_loc, "output_surv_SubOnly.txt"))




# Compare the models

print("model comparison subordinates")
comp <- compare(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10)
print(comp)


list_models <- list(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10)

for(i in 1:length(list_models)){
  target <- list_models[[i]]
  
  print(paste0("showing results for  survival subordinates s", i))
  print(show(target))
  print(paste0("precis for survival subordinates s", i))
  print(precis(target))
  
  pdf(file=paste0(path_loc, "survival subordinates", i, ".pdf"))
  print(plot(target))
  dev.off()
}

print(paste("test equal ", test))
print(paste("cores = ", cor_n))
print(paste("iterations = ", it))
print(paste("burn-in = ", wm))

info <- sessionInfo()


print(info)
sink(NULL)

