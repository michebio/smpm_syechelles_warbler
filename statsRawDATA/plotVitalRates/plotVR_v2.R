#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Script to create an alternative version of Fig. 3
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cairo_ps("statsRawDATA/plotVitalRates/vital_rates_withshades_v2.eps",
         fallback_resolution = 600)
par(mfrow=c(2,2), oma=c(0.5,1,0,0.2),mar=c(2.5,3.5,2,0.2))
print(plot(Surv.mean ~ age.mean,
           data = surv.psD, pch=20,xlab="", ylab="", ylim=c(0,1.01),
           xaxt="n", yaxt="n", xlim=c(0.5,15), col = "black"))
#xlab  =expression("age "*italic(a)), ylab = "Probability of surviving")
axis(1, at=1:15, labels=c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"), 
     mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=c(0,0.2,0.4,0.6,0.8,1), labels=c("0.0","0.2","0.4","0.6","0.8","1.0"),  
     mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.02)
mtext("age (6-months steps)",side=1,padj=1.5,cex=0.8)
mtext("survival probability", side=2, padj=-1.4, cex=1)
legend(x=1, y=0.6, legend=c("dominant", "helper", "non-helper"),
       lty=c(1,2,3), 
       pch=c(20,8, 5), lwd=c(1.5,1.5,1.5), 
       col=c("black",viridis(5)[5], viridis(5)[4]),bty = "n" )
text(14,0.96,labels="(a)")
n <- 100
age=seq(0,15,length.out=n)
shade(p_ci_H, plot_ages, col=col.alpha(viridis(5)[5], 0.3),border=NA)
shade(p_ci_AB, plot_ages, col=col.alpha(viridis(5)[4], 0.3),border=NA)
shade( mu.PI_Dom , plot_ages_d, col=col.alpha("gray71"),border=NA)

#qui devi caricare punti per H AB and sub in general
points(Surv.mean ~ age.mean,   data = surv.psD, pch=20, col = "black")
points(Surv.mean ~ age.mean,   data = surv.psH, pch=8,col=viridis(5)[5])
points(Surv.mean ~ age.mean,   data = surv.psAB, pch=5,col=viridis(5)[4])

lines(plot_ages, p_mu_H, col= viridis(5)[5], lwd= 1.5, lty=2) # same settings as in rep plot
lines(plot_ages, p_mu_AB, col = viridis(5)[4], lty=3, lwd =1.5)
lines( plot_ages_d, mu.median_Dom, lty=1, lwd=1.5, col= "black")
##########################################################################################
##########################################################################################
# dominant with a helper
#rm(list = ls())

print(plot(NH.mean ~ age.mean,
           data = numH.ps, pch=20,xlab="", ylab="", ylim=c(0,0.4), lwd=1.5,
           xaxt="n", yaxt="n", xlim=c(0.5,15), col="white"))
#xlab  =expression("age "*italic(a)), ylab = "Probability of surviving")
axis(1, at=1:15, labels=c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=c(0,0.2,0.4,0.6,0.8,1), labels=c("0.0","0.2","0.4","0.6","0.8","1.0"),  mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.02)
#mtext(expression("age, "*italic(a)),side=1,padj=1.2,cex=0.8)
mtext("age (6-months steps)",side=1,padj=1.5,cex=0.8)
mtext("helper presence \n probability", side=2, padj=-0.7, cex=1)
text(14,0.38,labels="(b)")
shade(p_ci_numH , plot_ages_numH , col=col.alpha("gray", 0.3),border=NA)
lines(plot_ages_numH , p_mu_numH, lwd=1.5)
points(NH.mean ~ age.mean,
       data = numH.ps, pch=20)
# plot 95% intervals (all prime numbers)

##########################################################################################
##########################################################################################
### Reproduction

print(plot(rec.mean ~ age.mean, lwd=1.5,
           data = recD.ps0, pch=0,xlab="", ylab="", ylim=c(0,0.65),
           xaxt="n", yaxt="n", xlim=c(0.5,15),col="white"))
#xlab  =expression("age "*italic(a)), ylab = "Probability of surviving")

axis(1, at=1:15, labels=c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=c(0,0.2,0.4,0.6,0.8,1), labels=c("0.0","0.2","0.4","0.6","0.8","1.0"),  mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.02)
#mtext(expression("age, "*italic(a)),side=1,padj=1.2,cex=0.8)
mtext("age (6-months steps)",side=1,padj=1.5,cex=0.8)
mtext("reproduction probability", side=2, padj=-1.4, cex=1)
legend(x=2, y=0.60, legend=c("dominant with no helper","dominant with 1+ helper",
                             "cobreeding helper"),lty=c(4,1,2), pch=c(0,2,8), 
       lwd=c(1.5,1.5,1.5),
       #col=c("black","black","gray71"),
       col=c(viridis(5)[1], viridis(5)[2], viridis(5)[5]),
       bty = "n" )
text(14,0.60,labels="(c)")
# plot the shades
shade(p_ci_recD0, plot_ages_d, col=col.alpha(viridis(5)[1], 0.3),border=NA)
shade(p_ci_recD1, plot_ages_d, col=col.alpha(viridis(5)[2], 0.3),border=NA)
shade(p_ci_rech, plot_ages, col=col.alpha(viridis(5)[5], 0.3),border=NA)

# do the lines on topof the shades
lines(plot_ages_d, p_mu_recD0, col = viridis(5)[1], lty=4,lwd=1.5)
lines(plot_ages_d, p_mu_recD1, col = viridis(5)[2],lwd=1.5)
lines(plot_ages, p_mu_rech, col = viridis(5)[5], lwd= 1.5, lty=2)
# plot 95% intervals (all prime numbers)
points(rec.mean ~ age.mean,data = recD.ps0, pch=0,col=viridis(5)[1])
points(rec.mean ~ age.mean,   data = recD.ps1H, pch=2,col=viridis(5)[2])#c("black"))
points(rec.mean ~ age.mean,   data = recH.ps, pch=8,col=viridis(5)[5])#c("gray71"))
##########################################################################################
##########################################################################################
### entering as helper
#entering as helper
print(plot(rH.mean ~ momage_count.mean,lwd=1.5,
           data = ent.ps, pch=20,xlab="", ylab="", ylim=c(0,1.01),
           xaxt="n", yaxt="n", xlim=c(0.5,15)))
#xlab  =expression("age "*italic(a)), ylab = "Probability of surviving")
axis(1, at=1:15, labels=c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=c(0,0.2,0.4,0.6,0.8,1), labels=c("0.0","0.2","0.4","0.6","0.8","1.0"),  mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.02)
mtext("mother age (6-months steps)",side=1,padj=1.5,cex=0.8)

mtext("probability of an offspring \n becoming a helper", side=2, padj=-0.7, cex=1)
text(14,0.95,labels="(d)")

lines(plot_ages, p_mu_ent, lwd=1.5)
shade(p_ci_ent, plot_ages, col=col.alpha("gray", 0.3),border=NA)



dev.off()
