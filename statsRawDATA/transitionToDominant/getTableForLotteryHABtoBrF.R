#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Code to get a table with number of sites available and if a H or AB takes this position.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rm(list=ls())
dat <- read.table("statsRawDATA/data_year_2019.txt")

b <- dat[which(dat$Status=="BrF" & dat$Surv==0 & dat$age_count>0.5),]
b$deaths <- ave(b$Surv, b$SeasonID, FUN=length)
b <- unique(b[,c("SeasonID","deaths")])


dat <- read.table("statsRawDATA/data_year_2019.txt")   # read statuses table

dat <- dat[which(dat$Surv==1),]
dat <- dat[!is.na(dat$NextStatus),]
dat <- dat[which(dat$Status=="H" | dat$Status=="AB" | dat$Status=="NSA"),] # choose H and AB at time t
dat$BecomesBrF <- rep(0,nrow(dat))
dat[dat$NextStatus=="BrF","BecomesBrF"] <- 1
dat[dat$NextStatus!="BrF","BecomesBrF"] <- 0
dat <- dat[,c("BirdID","Status","SeasonID","NextStatus","BecomesBrF","totSub", "age")]
dat1 <- merge(dat, b, all=T)
dat <- dat1[!is.na(dat1$BirdID),]
summary(dat)
temp <- dat


# group by!
dat[which(dat$Status=="H" & dat$BecomesBrF==1), "H_successfull"] <- 1
dat[which(dat$Status=="H"), "isH"] <- 1  
dat[which(dat$Status=="H" & dat$BecomesBrF==0), "H_fail"] <- 1

dat[which(dat$Status=="AB" & dat$BecomesBrF==1), "AB_successfull"] <- 1
dat[which(dat$Status=="AB"), "isAB"] <- 1  
dat[which(dat$Status=="AB" & dat$BecomesBrF==0), "AB_fail"] <- 1

dat[which(dat$Status=="NSA" & dat$BecomesBrF==1), "NSA_successfull"] <- 1
dat[which(dat$Status=="NSA"), "isNSA"] <- 1  
dat[which(dat$Status=="NSA" & dat$BecomesBrF==0), "NSA_fail"] <- 1

sum.x <- function(x) sum(x, na.rm = T)
dat$NumberOfHelpers <- ave(dat$isH, dat$SeasonID, FUN = sum.x)
dat$NumberOfHelpersSuccessful <- ave(dat$H_successfull, dat$SeasonID, FUN = sum.x)
dat$NumberOfHelpersNotSuccessful <- ave(dat$H_fail, dat$SeasonID, FUN = sum.x)

dat$NumberOfAB <- ave(dat$isAB, dat$SeasonID, FUN = sum.x)
dat$NumberOfABSuccessful <- ave(dat$AB_successfull, dat$SeasonID, FUN = sum.x)
dat$NumberOfABNotSuccessful <- ave(dat$AB_fail, dat$SeasonID, FUN = sum.x)

dat$NumberOfGenericSub <- ave(dat$isNSA, dat$SeasonID, FUN = sum.x)
dat$NumberOfGenericSubSuccessful <- ave(dat$NSA_successfull, dat$SeasonID, FUN = sum.x)
dat$NumberOfGenericSubNotSuccessful <- ave(dat$NSA_fail, dat$SeasonID, FUN = sum.x)

cols <- c("SeasonID", "NumberOfHelpers", "NumberOfHelpersSuccessful", "NumberOfAB",
          "NumberOfABSuccessful", "NumberOfGenericSub", "NumberOfGenericSubSuccessful")
out <- unique(dat[,c("SeasonID", "NumberOfHelpers", "NumberOfHelpersSuccessful", "NumberOfHelpersNotSuccessful",
                     "NumberOfAB",
                     "NumberOfABSuccessful", 
                     "NumberOfABNotSuccessful", "NumberOfGenericSub", "NumberOfGenericSubSuccessful",
                     "NumberOfGenericSubNotSuccessful")])
(out$NumberOfABNotSuccessful + out$NumberOfABSuccessful)==out$NumberOfAB
out$NumberOfHelpers == (out$NumberOfHelpersNotSuccessful + out$NumberOfHelpersSuccessful)
dim(out)
length(unique(out$SeasonID))
tot = sum(out$NumberOfHelpers) + sum(out$NumberOfAB)
r_out <- data.frame(SeasonID = numeric(tot), BecomesBrF=numeric(tot), 
                    Status= factor(tot, levels=c("H", "AB")), TerrAv = numeric(tot),
                    NumberOfHelpers = numeric(tot), NumberOfAB = numeric(tot))
counter <- cumsum(out$NumberOfAB+out$NumberOfHelpers)
for(i in 1:nrow(out)){
  #print(i)
  dims <- out[i,"NumberOfHelpers"]
  h1 <- data.frame(SeasonID = rep(out[i,"SeasonID"], dims), 
                   BecomesBrF=c(rep(1,out[i, "NumberOfHelpersSuccessful"]), rep(0, out[i, "NumberOfHelpersNotSuccessful"])),
                   Status = rep("H", dims),
                   TerrAv = rep((out[i, "NumberOfHelpersSuccessful"]+out[i, "NumberOfABSuccessful"]), dims),
                   NumberOfHelpers = rep(out[i,"NumberOfHelpers"], dims), NumberOfAB = rep(out[i,"NumberOfAB"], dims))
  dims <- out[i,"NumberOfAB"]
  ab1 <- data.frame(SeasonID = rep(out[i,"SeasonID"], dims), 
                    BecomesBrF=c(rep(1,out[i, "NumberOfABSuccessful"]), 
                                 rep(0, out[i, "NumberOfABNotSuccessful"])),
                    Status = rep("AB", dims),
                    TerrAv = rep((out[i, "NumberOfHelpersSuccessful"]+out[i, "NumberOfABSuccessful"]), dims),
                    NumberOfHelpers = rep(out[i,"NumberOfHelpers"], dims), NumberOfAB = rep(out[i,"NumberOfAB"], dims))
  if(nrow(rbind(h1, ab1))>0){
    if(i==1){
      r_out[1:counter[i],] <- rbind(h1, ab1)
    } else {
      r_out[(1+counter[i-1]):counter[i],] <- rbind(h1, ab1)
    }
  }
}

write.table(r_out,"statsRawDATA/transitionToDominant/HandABthatBecomeBrF.txt")
r_out <- read.table("statsRawDATA/transitionToDominant/HandABthatBecomeBrF.txt")
dim(r_out)
