#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Code to estimate transition probabilities of helpers and non-helpers to dominant breeder.
# Parameter estimate of the difference between helpers and non-helpers
# is used to run a lottery pool from which individuals will be picked to become 
# breeding females in the SMPM.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source("statsRawDATA/transitionToDominant/getTableForLotteryHABtoBrF.R")
rm(list=ls())
d <- read.table("statsRawDATA/transitionToDominant/HandABthatBecomeBrF.txt")
d <- d[,c("TerrAv","Status","BecomesBrF","SeasonID","NumberOfHelpers","NumberOfAB")]
d <- d[complete.cases(d),]
d <- d[d$TerrAv!=0,]
d <- d[d$Status!="NSA",]
d$isH <- ifelse(d$Status=="H", 1, 0)
d <- d[order(d$SeasonID, d$Status, d$BecomesBrF),]
names(d) <- c("terrAv", "Status", "transitionsProb","SeasonID", "h", "ab", "isH")
d<- d[,c('transitionsProb','terrAv','isH','h','ab')]
### modification with log: log(p)=min[0, value]
mylog<-function(p){
  return(log(p))
}
myexp<-function(z){
  return(pmin((exp(z)),1))
}
#now set up a likelihood function
mylog.log.like<-function(par,dat){
  cont <- par['cont']
  risk <- myexp(cont*dat$isH+log(dat$terrAv)-log(exp(cont)*dat$h + dat$ab))
  return(sum(log(risk[dat$transitionsProb==1]))+sum(log(1-risk[dat$transitionsProb!=1])))
}
#optimize it
like.res<-optim(par=c(cont=-0.7),fn=mylog.log.like,dat=d,method='BFGS',
                control=list(fnscale=-1,reltol=1e-50),hessian=T)
like.res$par
print(like.res$par)
par_1 <- like.res$par#-0.640


fisher_info<-solve(-like.res$hessian)
prop_sigma<-sqrt(diag(fisher_info))
#prop_sigma<-diag(prop_sigma)
upper<-like.res$par+1.96*prop_sigma
lower<-like.res$par-1.96*prop_sigma
interval<-data.frame(value=like.res$par, upper=upper, lower=lower)
sink(file="statsRawDATA/transitionToDominant/results_HandABthatBecomeBrF.txt")
print("RESULTS: PROBABILITY OF HELPERS AND NON HELPERS TO BECOME BREEDERS")
print("results")
print(interval)
print("prop_sigma==")
print(prop_sigma)
#with these results try to simulate some data
print("testing by simulating new data...")
nuHTransitioning <- NULL
nuABTransitioning <- NULL
tot <- NULL
for (i in 1:1000){
  totH <- 40
  totAB <- 10
  TerrAv <- 10
  pH<-myexp(log(TerrAv)+ par_1-log(exp(par_1)*totH+totAB))
  pAB <- myexp(log(TerrAv)-log(exp(par_1)*totH+totAB))
  nH <- sum(rbinom(n=totH, prob=pH, size=1))
  nAB <- sum(rbinom(n=totAB, prob=pAB, size=1))
  nuHTransitioning[i] <- nH
  nuABTransitioning[i] <- nAB
  tot[i] <- nH+nAB
}
print(" with totH <- 40;
  totAB <- 10;
  TerrAv <- 10")
print("transitioning helpers")
print(mean(nuHTransitioning))
print("transitioning non-helpers")
print(mean(nuABTransitioning))
print("tot")
print(mean(tot))
sink(NULL)
