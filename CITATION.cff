# This CITATION.cff file was generated with cffinit.
# Visit https://bit.ly/cffinit to generate yours today!

cff-version: 1.2.0
title: >-
  Population-level consequences of facultatively
  cooperative behaviour in a stochastic environment
message: Please cite this dataset using these metadata.
type: software and data
authors:
  - given-names: Michela
    family-names: Busana
    email: m.busana@rug.nl
    affiliation: 'University of Groningen '
    orcid: 'https://orcid.org/0000-0002-3806-8575'
  - given-names: Dylan Z
    family-names: Childs
    orcid: 'https://orcid.org/0000-0002-0675-4933'
  - given-names: Terry
    family-names: Burke
    orcid: 'https://orcid.org/0000-0003-3848-1244'
  - given-names: Jan
    family-names: Komdeur
    orcid: 'https://orcid.org/0000-0002-9241-0124'
  - given-names: David S
    family-names: Richardson
    orcid: 'https://orcid.org/0000-0001-7226-9074'
  - given-names: Hannah L
    family-names: Dugdale
    orcid: 'https://orcid.org/0000-0001-8769-0099'
identifiers:
  - type: doi
    value: 10.34894/UWUGZH
    description: Data and software related to the publication
  - type: doi
    value: 10.1111/1365-2656.13618
    description: Open source publication
repository-code: 'https://gitlab.com/michebio/smpm_syechelles_warbler'
abstract: |2

      The social environment in which individuals live affects their fitness and in turn population dynamics as a whole. Birds with facultative cooperative breeding can live in social groups with dominants, subordinate helpers that assist with the breeding of others, and subordinate non-helpers. Helping behaviour benefits dominants through increased reproductive rates and reduced extrinsic mortality, such that cooperative breeding might have evolved in response to unpredictable, harsh conditions affecting reproduction and/or survival of the dominants. Additionally, there may be different costs and benefits to both helpers and non-helpers, depending on the time-scale. For example, early-life costs might be compensated by later-life benefits. These differential effects are rarely analysed in the same study.
      We examined whether helping behaviour affects population persistence in a stochastic environment and whether there are direct fitness consequences of different life-history tactics adopted by helpers and non-helpers.
      We parameterised a matrix population model describing the population dynamics of female Seychelles warblers Acrocephalus sechellensis, birds that display facultative cooperative breeding. The stochastic density-dependent model is defined by a (st)age structure that includes life-history differences between helpers and non-helpers and thus can estimate the demographic mechanisms of direct benefits of helping behaviour.
      We found that population dynamics are strongly influenced by stochastic variation in the reproductive rates of the dominants, that helping behaviour promotes population persistence and that there are only early-life differences in the direct fitness of helpers and non-helpers.
      Through a matrix population model, we captured multiple demographic rates simultaneously and analysed their relative importance in determining population dynamics of these cooperative breeders. Disentangling early-life versus lifetime effects of individual tactics sheds new light on the costs and benefits of helping behaviour. For example, the finding that helpers and non-helpers have similar lifetime reproductive outputs and that differences in reproductive values between the two life-history tactics arise only in early life suggests that overall, helpers and non-helpers have a similar balance of costs and benefits when analysing direct benefits. We recommend analysing the consequence of different life-history tactics, during both early life and over the lifetime, as analyses of these different time frames may produce conflicting results.
license: Apache-2.0
version: V1
