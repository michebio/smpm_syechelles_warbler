p268806@peregrine:code_matrixpopmod nano 

postscript(paste0(plot_loc, "violinplot_population_parameters_numSub.eps"))
# version 2, simplified:
par(mfrow=c(5,1), mar=c(1,1,1,1),oma=c(2,2,2,2))
x <- factor(c(rep("observed", length(ps.obs$Psize)), 
              # rep("IBM", length(ps.sim$p.size)),
              rep("SMPM", length(ps.pred))))
x <- factor(x,levels(x)[c(2,1,3)])
data<- data.frame(y= c(ps.obs$Psize, #ps.sim$p.size, 
                       ps.pred), grp= x)
vioplot(formula=y~grp, data=data, col=viridis(2), ylab= "", xlab="",
        xaxt="n", yaxt="n",drawRect=T,
        lty=1,lwd=1)
#lines(x=1:m, y=repAB[1,(1+m*2):(m*3)], col="gray", lty=5)
axis(1,  labels=c("observed", #"IBM", 
                  "SMPM"), at=1:2,mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=seq(110,range(data$y)[2], by=30),
     mgp=c(-0.1,-1.5,0), padj=0.2, tck=0.02, cex=.8)
#axis(3, at=1:3, labels=c("observed", "IBM", "SMPM")) #, mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
mtext("number of females", side=2, padj=-1, cex=.8)
text(3, 200, adj = c(-6.5,0), "(a)")
# num of sub
x <- factor(c(rep("observed", length(sub)), 
              #rep("IBM", length(sub_ibm)),
              rep("SMPM", length(pred_sub))))
#x <- factor(x,levels(x)[c(2,1,3)])
data<- data.frame(y= c(sub, #sub_ibm, 
                       pred_sub), grp= x)
vioplot(formula=y~grp, data=data, col=viridis(2), ylab= "", xlab="",
        xaxt="n", yaxt="n",drawRect=T,
        lty=1,lwd=1)
#lines(x=1:m, y=repAB[1,(1+m*2):(m*3)], col="gray", lty=5)
axis(1, at=1:2, labels=c("observed", #"IBM", 
                         "SMPM"), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=seq(10,range(data$y,na.rm=T)[2], by=30),
     #seq(round(range(data$y, na.rm=T)[1]), round(range(data$y, na.rm=T)[2]), by=0.25),
     mgp=c(-0.1,-1.5,0), padj=0.2, tck=0.02, cex=.8)
#mtext("number of subordinates", side=2, padj=-1, cex=.8)
mtext("number of \n subordinates", side=2, padj=-0.2, cex=0.8)
text(3, 90, adj = c(-6.5,0), "(b)")
# LRS gen
x <- factor(c(rep("observed", length(lrs[,"sumOff"])), 
              #rep("IBM", length(LRs_ibm_all$LRS)),
              rep("SMPM", length(IPM.sim$LRS[3,]))
))
#x <- factor(x,levels(x)[c(2,1, 3)])
data<- data.frame(y= c(lrs[,"sumOff"], #LRs_ibm_all$LRS, 
                       rnorm(length(IPM.sim$LRS[3,]), 1,0.1)), grp= x)
vioplot(formula=y~grp, data=data, col=c(viridis(2)[1],#"white", 
                                        "white"), lineCol=c("black",#"white",
                                                            "white"), 
        ylab= "", xlab="",
        xaxt="n", yaxt="n",drawRect=T, lty=c(1,1,0),lwd=1, border=c("black",#"white",
                                                                    "white"), 
        rectCol=c("black","white","white"))
#lines(x=1:m, y=repAB[1,(1+m*2):(m*3)], col="gray", lty=5)
axis(1, at=1:2, labels=c("observed", #" ", 
                         " "), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=seq(round(range(data$y, na.rm=T)[1]),round(range(data$y, na.rm=T)[2]), by=2),
     mgp=c(-0.1,-1.5,0), padj=0.2, tck=0.02, cex=.8)
mtext("LRS of \n subordinates", side=2, padj=-0.2, cex=0.8)
text(3, 7.5, adj = c(-6.5,0), "(c)")

# LRS helpers
x <- factor(c(rep("observed", length(lrs[lrs$wasH==1,"sumOff"])), 
              #rep("IBM", length(lrs_ibm_h$LRS)),
              rep("SMPM", length(IPM.sim$LRS[1,]))))


data<- data.frame(y= c(lrs[lrs$wasH==1,"sumOff"],# lrs_ibm_h$LRS, 
                       IPM.sim$LRS[1,] ), grp= x)
vioplot(formula=y~grp, data=data, col=viridis(2), ylab= "", xlab="",
        xaxt="n", yaxt="n",drawRect=T,
        lty=1,lwd=1)
#lines(x=1:m, y=repAB[1,(1+m*2):(m*3)], col="gray", lty=5)
axis(1, at=1:2, labels=c("observed", #"IBM", 
                         "SMPM"), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=seq(round(range(data$y, na.rm=T)[1]),round(range(data$y, na.rm=T)[2]), by=2),
     mgp=c(-0.1,-1.5,0), padj=0.2, tck=0.02, cex=.8)
mtext("LRS of helpers", side=2, padj=-1.2, cex=0.8)
text(3, 7.5, adj = c(-6.5,0), "(d)")

#LRS non-helpers
x <- factor(c(rep("observed", length(lrs[lrs$wasAB==1,"sumOff"])), 
              #rep("IBM", length(lrs_ibm_ab$LRS)),
              rep("SMPM", length(IPM.sim$LRS[2,]))))
#x <- factor(x,levels(x)[c(2,1,3)])
data<- data.frame(y= c(lrs[lrs$wasAB==1,"sumOff"], #lrs_ibm_ab$LRS, 
                       IPM.sim$LRS[2,]), grp= x)
vioplot(formula=y~grp, data=data, col=viridis(2), ylab= "", xlab="",
        xaxt="n", yaxt="n",drawRect=T,
        lty=1,lwd=1)
#lines(x=1:m, y=repAB[1,(1+m*2):(m*3)], col="gray", lty=5)
axis(1, at=1:2, labels=c("observed", #"IBM", 
                         "SMPM"), mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(2, at=seq(round(range(data$y, na.rm=T)[1]),round(range(data$y, na.rm=T)[2]), by=2),
     mgp=c(-0.1,-1.5,0), padj=0.2, tck=0.02, cex=.8)
mtext("LRS of non-helpers", side=2, padj=-1.2, cex=.8)
text(3, 7, adj = c(-6.5,0), "(e)")
dev.off()
