cairo_ps("population_model/stocModelOutput/plots/repVal_withShades_colors_CI.eps", fallback_resolution = 600)
#pdf("repVal_withShades.pdf")
par(mfrow=c(1,1), mar=c(1,1,1,1),oma=c(2,2,2,2))
plot(x=1:m,y=stRepH,col="white", type="l", ylim=c(0,0.245), xaxt="n", yaxt="n",xlab="",
     ylab="", lty=3,lwd=2)
#lines(x=1:m, y=repAB[1,(1+m*2):(m*3)], col="gray", lty=5)

axis(1, at=seq(1,30,by=1), labels = c("","","","","","","","","","","","","","",
                                      "","","","","","","","","","","","","","","",""), 
     mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.02)
axis(1, at=seq(2,30,by=4), labels=c("1","3","5","7","9","11","13","15"), 
     mgp=c(-0.1,-1.5,0), padj=2.5,tck=0.025)

axis(2, at=seq(0,0.24,by=0.02), labels=c("0.00","0.02","0.04","0.06","0.08","0.10","0.12","0.14","0.16","0.18", "0.20","0.22","0.24"),
     mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.02)
mtext("age (6-months steps)",side=1,padj=2,cex=1)
mtext("reproductive values", side=2, padj=-2, cex=1)
polygon(c(1:m, rev(1:m)), c(ciab["lower",], rev(ciab["upper",])),
        col = alpha(viridis(5)[4],0.6), border = NA)
polygon(c(1:m, rev(1:m)), c(cih["lower",], rev(cih["upper",])),
        col = alpha(col=viridis(5)[5],0.6), border = NA)
lines(x=1:m,y=stRepH,col=viridis(5)[5],lty=3,lwd=2)
lines(x=1:m,y=stRepAB,col=viridis(5)[4],lty=2,lwd=2)

#lines(x=1:m,y=meanh,col="gray76",lty=3,lwd=2)
#lines(x=1:m,y=meanab,col="black",lty=2,lwd=2)
legend(legend=c("helpers (h)","non-helpers (u)"), x=8,y=0.12,lwd=c(2,2), col=c(viridis(5)[5],viridis(5)[4]),
       lty=c(3,2), cex=1,merge = TRUE,bty ="n")
dev.off()
