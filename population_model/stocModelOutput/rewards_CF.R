# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Plot coefficient of variation of LRS calculated with rewards 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cfs <- apply(coef_var_ro, 1, mean, na.rm=T)
cfs_H <- c(cfs[31], cfs[2:30]+cfs[32:60])
cfs_AB <- c(cfs[61], cfs[2:30]+cfs[62:90])
# plot the statistics (mean, variance and coefficient )
qq <- apply(coef_var_ro - cfs, 1, quantile, probs=c(0.25, 0.75), na.rm=T)
q_diff25 <- cfs-qq[1,];
q_diff75 <- cfs+qq[2,]; 
qH_diff25<- c(q_diff25[31], q_diff25[2:30]-q_diff25[32:60])
qH_diff75 <- c(q_diff75[31], q_diff75[2:30]+q_diff75[32:60])
qAB_diff25 <- c(q_diff25[61], q_diff25[2:30]-q_diff25[62:90])
qAB_diff75 <- c(q_diff75[61], q_diff75[2:30]+q_diff75[62:90])

ats_x <- seq(2,29, by=4)
ats_y <- seq(0,max(coef_var_ro,na.rm=T),by=2)
y_ran <- range(coef_var_ro,na.rm=T)
plot(cfs_H[1:30], type="l",  ylim=c(0, y_ran[2]+0.3),xlab="", ylab="",
     xlim =c(0, length(ages)),
     xaxt="n", yaxt="n",  col= "white")

axis(1, at=ats_x, labels=ages[ats_x], mgp=c(-0.1,-1.5,0),
     padj=2.5,tck=0.03)
axis(1, at=1:30, labels=rep("",30), mgp=c(-0.1,-1.5,0),
     padj=2.5,tck=0.02)
axis(2, at=ats_y, labels=ats_y,#c("10","30","50","70","90","110","130","150"),  
     mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.03)
mtext("age (6 months steps)",side=1,padj=2,cex=1.5)
mtext("coefficient of variation",side=2,padj=-1,cex=1.5)


polygon(c(1:length(qH_diff75), rev(1:length(qH_diff75))), c((qH_diff75), rev(qH_diff25)),
        col = alpha(viridis(5)[5],0.6), border = NA)
polygon(c(1:length(qAB_diff75), rev(1:length(qAB_diff75))), c((qAB_diff75), rev(qAB_diff25)),
        col = alpha(viridis(5)[4],0.6), border = NA)

lines(cfs_H, col=viridis(5)[5], lty=4,lwd=2.5)
lines(cfs_AB, col=viridis(5)[4], lty=3,lwd=2.5)

x_pos <- 4
y_pos <- max(y_ran)-1
text(28,max(y_ran),labels="(c)")
legend(x = x_pos, y=y_pos, legend = c("helpers", "non-helpers"), 
       #title=c("observed predicted"),
       col=c(viridis(5)[5],
             viridis(5)[4]), 
       lty=c(4,3), 
       seg.len = 5,
       bty='n')
