#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Plot of number of individuals predicted by the IBMs.
# Variations between simulations are represented by different lines
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


DTs <- fread(file = "population_model/IBM/outputs/summary_combined.csv")

n.yrs <- max(DTs$yr)
burningIn <- n.yrs - 100
lenpl <- burningIn:n.yrs
DTs <- DTs[yr > burningIn,]
ran_plot <- range(DTs$nbt, DTs$nht, DTs$nabt, na.rm=2)
pprp <- 0.010 #tweak number to find a good position of the plot legend
cairo_ps("population_model/IBM/plots/IBM_numInd_variation_between_repetitions.eps", fallback_resolution = 600)

layout(1)
par(mar=c(1,1,1,1),oma=c(2,2,2,2))
ats_x <- seq(burningIn,n.yrs,by=20)
ats_x1 <- seq(burningIn,n.yrs,by=1)
ats_y <- seq(ran_plot[1],ran_plot[2], by=50)
plot(DTs$mean_nbt, type="l", ylim=c(ran_plot[1]-2,ran_plot[2]+pprp*ran_plot[2]),xlab="", ylab="",
     
     xlim=c(burningIn,n.yrs),#nrow(DTs[repetition==min(DTs$repetition),])
     xaxt="n", yaxt="n", pch=15, col= "white")

axis(1, at=ats_x, labels=ats_x, mgp=c(-0.1,-1.5,0),
     padj=2.5,tck=0.03)
axis(1, at=ats_x1, labels=rep("",length(ats_x1)), mgp=c(-0.1,-1.5,0),
     padj=2.5,tck=0.02)
axis(2, at=ats_y, labels=ats_y,#c("10","30","50","70","90","110","130","150"),  
     mgp=c(-0.1,-1.5,0), padj=-2.5, tck=0.03)
mtext("time (6 months steps)",side=1,padj=2,cex=1.5)
mtext("number of females",side=2,padj=-2,cex=1.5)

# points(data$totD, col = "black", pch = 20)
# points(data$totSub,  pch=10,col=viridis(5)[3])
# points(data$totH,pch=8,col=viridis(5)[5])
# points(data$totAB, pch=5,col=viridis(5)[4] )

for (i in min(DTs$repetition):max(DTs$repetition)){
tmp <- subset(DTs, repetition==i)
lines(x=tmp$yr, y=tmp$nbt, col= "black", lty=3,lwd=1)
lines(x=tmp$yr, y=tmp$nht,  col=viridis(5)[5], lwd= 1, lty=3)
lines(x=tmp$yr, y=tmp$nabt,  col=viridis(5)[4], lwd= 1, lty=3)
}
#lines(c(sim$mean_nht+sim$mean_nabt)[2:(lenpl+2)], col=viridis(5)[3],lty=3,lwd=1.5)

# use position data of previous legend to draw legend with invisble lines and points but with labels and box. x.intersp controls distance between lines and labels
legend(x = round(n.yrs-n.yrs*0.08), y = ran_plot[2], legend = c("dominants",
                                          "helpers",
                                          "non-helpers"),  
       title = "Number of females predicted by the IBMs",
       col=c( "black", viridis(5)[5],viridis(5)[4]),
       lty=rep(3,3),lwd=rep(1,3),
       ncol=1,  bty='n', cex = 1.3)

dev.off()

