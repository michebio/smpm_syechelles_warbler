#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Generate Plots for the IBM
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print("running plots script")
options(rgl.useNULL=TRUE)
library(rgl)

library(vioplot)#
library(scales)#to allow for color transparency
library(viridis)
library(Cairo)
library(data.table)
library(plotrix)
library(export)
library(withr)

print("libraries loaded")

plot_loc <- "population_model/IBM/plots/"
if(dir.exists(plot_loc)) unlink(plot_loc, recursive=TRUE)
if(!dir.exists(plot_loc)) dir.create(plot_loc)
source("population_model/stocModelOutput/functions_SMPM.R")
source("statsRawDATA/calcLRSfromDATA.R")
source("population_model/IBM/3_summarizeData.R")
print("done with source ")
d <- read.table("statsRawDATA/data_year_2019.txt")

max.age <- 15
N_mean <- unique(d$Psize_mean); print(paste("N mean ", N_mean))
N_sd <- unique(d$Psize_sd); print(paste("N sd ", N_sd))


i.par <- mk_intpar(m=30, minAge=0.5, maxAge=15)
wz <- fread("population_model/stocModelOutput/output/wz.csv")
vz <- fread("population_model/stocModelOutput/output/vz.csv")
wt <- rep(1, times=i.par$m*3)
m <- i.par$m
burnIn <- 7000
numSim<- dim(wz)[2]
m<-i.par$m

d <- read.table("statsRawDATA/data_year_2019.txt")

load("population_model/stocModelOutput/output/IPM.sim.RData")
# sim.data <- read.table("./population_model/sim.data_withLRS.txt",header=T)


# psize
ps.obs <- unique(d[!is.na(d$Psize), c("SeasonID", "Psize")])
ps.obs$Psize
ps.sim_mean <- unique(unique_summary[,c("repetition", "mean_psize", "sd_psize")])
ps.sim <- unique(sim.data[,c("year", "repetition", "p.size", "ab.size", "h.size", "b.size")])
n <- wz[,burnIn:numSim]
ps.pred <- apply(n,2, sum)

# num dominants
dom <- unique(d[, c("totD", "totSub", "SeasonID")])
dom$totD
dom.sim_mean <- unique(unique_summary[,c("repetition", "mean_nbt", 
                                    "mean_nht", 
                                    "mean_nabt")])
dom.sim <- ps.sim[,c("year", "repetition", "b.size")]
# dom.sim$b.size
wz <- IPM.sim$wz
n <- wz[1:m,burnIn:numSim]
b <- apply(n,2, sum)

# number of sub/dom
sub_dom <- dom$totSub/dom$totD
sub_dom_ibm_mean <- (dom.sim$mean_nht+dom.sim$mean_nabt)/dom.sim$mean_nbt
sub_dom_ibm <- (ps.sim$h.size+ps.sim$ab.size)/ps.sim$b.size
n <- wz[c(m+1):(3*m),burnIn:numSim]
s <- apply(n,2, sum)
pred_sub_dom <- s/b

# number of sub
sub <- dom$totSub
sub_ibm_mean <- (dom.sim$mean_nht+dom.sim$mean_nabt)
sub_ibm <- ps.sim$h.size+ps.sim$ab.size
n <- wz[c(m+1):(3*m),burnIn:numSim]
s <- apply(n,2, sum)
pred_sub <- s

# lrs
#toDO!
lrs$sumOff
lrs_ibm <- sim.LRS

# lrs helpers
lrs[lrs$wasH==1,"sumOff"]
lrs_ibm_h <- sim.LRS$mean_LRS_H
#IPM.sim$LRS[1,]
# lrs non helper
lrs[lrs$wasAB==1,"sumOff"]
lrs_ibm_ab <- sim.LRS$mean_LRS_AB
#IPM.sim$LRS[2,]


source("population_model/IBM/plot_IBM_violinplot_population_parameters_numSub.R")
source("population_model/IBM/plot_IBM_violinplot_population_parameters.R")
source("population_model/IBM/plot_IBM_violinplot_population_parameters_only_MEANS.R")
## Plots
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##
## Plot the stable age distributions in each time step
##

i.par <- mk_intpar(m=30, minAge=0.5, maxAge=15)
wz <- fread("population_model/stocModelOutput/output/wz.csv")
vz <- fread("population_model/stocModelOutput/output/vz.csv")
wt <- rep(1, times=i.par$m*3)
m <- i.par$m
max.age <- 15
#library(ggplot2)
#library(popbio)
###############################################################################
##
## PLOT: number of individuals in each class
##
burnin <- 7000
B <- wz[1:i.par$m,burnin:ncol(wz)]
H <- wz[(i.par$m+1):(i.par$m*2),burnin:ncol(wz)]
AB <- wz[(i.par$m*2+1):(i.par$m*3),burnin:ncol(wz)]
B.size <- apply(B, 2, sum)
H.size <- apply(H, 2, sum)
AB.size <- apply(AB, 2, sum)
Sub.size <- apply(wz[(i.par$m+1):(i.par$m*3),burnin:ncol(wz)], 2, sum)


data <- read.table("statsRawDATA/data_year_2019.txt", header=T)
data <- unique(data[!is.na(data$totD),c("SeasonID","Psize","totD","totAB","totH","totSub")])
data[is.na(data$totAB2), "totAB2"] <- data[is.na(data$totAB2), "totAB"]
data[which(data$totAB==0), "totAB"] <- NA
data$totNSA <- data$totSub - data$totH - data$totAB
data <- data[order(data[,"SeasonID"]),]
data <- data[!is.na(data$totH),]
sim <- unique_summary[, c("mean_nbt", "mean_nht", "mean_nabt", "mean_terrAv", "repetition")]
sim <- sim[order(sim[,"repetition"]),]
# 3 separate graphs in the same page

############################################################################################################################
# altogether
data <- data[order(data$SeasonID),]
dummy <- data.frame(SeasonID=min(data$SeasonID):max(data$SeasonID), newtotD=NA)
data <- merge(data, dummy, all=T)


fp <- read.table("statsRawDATA/toWriteSeasonIDs_year.txt")
dim(data)
data <- merge(data,fp,all.x=T)
dim(data)
data$year <- as.numeric(gsub("\\D", "", data$toWrite))

data <- data[order(data$SeasonID),]


###### claculate quantiles
n <- 250
start=900 # adjust this value if you need to plot a different bit of the simulation
qB <- quantile(B.size-mean(B.size), probs=c(0.25, 0.75))
qB_diff25 <- B.size[start:(start + length(data$totD))]-qB[1]
qB_diff75 <- B.size[start:(start + length(data$totD))]+qB[2]
sub <- c(H.size+AB.size)
subLine <- c(H.size[start:(start + length(data$totD))]+
               AB.size[start:(start + length(data$totD))])
qS <- quantile(sub-mean(sub), probs = c(0.25,0.75))
qS_diff25 <- subLine-qS[1]
qS_diff75 <- subLine-qS[2]

subH <- H.size
subLine <- H.size[start:(start + length(data$totD))]
qH <- quantile(subH-mean(subH), probs = c(0.25,0.75))
qH_diff25 <- subLine-qH[1]
qH_diff75 <- subLine-qH[2]

subAB <- AB.size
subLine <- AB.size[start:(start + length(data$totD))]
qAB <- quantile(subAB-mean(subAB), probs = c(0.25,0.75))
qAB_diff25 <- subLine-qAB[1]
qAB_diff75 <- subLine-qAB[2]

subt <- Sub.size
subLine <- Sub.size[start:(start + length(data$totD))]
qS <- quantile(subt-mean(subt), probs = c(0.25,0.75))
qS_diff25 <- subLine-qS[1]
qS_diff75 <- subLine-qS[2]

#source("population_model/IBM/plot_IBM_numInd_withShades.R")
source("population_model/IBM/IBM_numInd_variation_between_repetitions.R")
source("population_model/IBM/IBM_numInd_shades.R")
################################################################################
##
## PLOT histograms and stable stage distribution
##
# histogram of ages and stable age distribution in each class

d <- read.table("statsRawDATA/data_year_2019.txt")

mesh <- meshpts <- i.par$meshpts
burnIn <- 7000
numSim<- dim(wz)[2]
m<-i.par$m


b <- wz[1:m,burnIn:numSim]
c <- apply(b, 2, sum)

for(j in 1L:ncol(b)){
  set(b,j=j, value = b[[j]]/c[j])
}
quarb <- apply(b, 1, quantile, probs=c(0.25, 0.75))
n <- apply(b,1,sum)
b <- n/sum(n)

h <- wz[(m+1):(2*m),burnIn:numSim]
c <- apply(h,2, sum)
for(j in 1L:ncol(h)){
  set(h,j=j, value = h[[j]]/c[j])
}
quarh <- apply(h, 1, quantile, probs=c(0.25, 0.75))
n <- apply(h,1,sum)
h <- n/sum(n)

ab <- wz[(2*m+1):(3*m),burnIn:numSim]
c <- apply(ab,2, sum)
for(j in 1L:ncol(ab)){
  set(ab,j=j, value = ab[[j]]/c[j])
}
quarab <- apply(ab, 1, quantile, probs=c(0.25, 0.75))
n <- apply(ab,1,sum)
ab <- n/sum(n)

sub <- wz[(m+1):(2*m),burnIn:numSim] + wz[(2*m+1):(3*m),burnIn:numSim]
c <- apply(sub,2, sum)
for(j in 1L:ncol(sub)){
  set(sub,j=j, value = sub[[j]]/c[j])
}
quarsub <- apply(sub, 1, quantile, probs=c(0.25, 0.75))
n <- apply(sub,1,sum)
sub <- n/sum(n)
# fix raw data
d <- read.table("statsRawDATA/data_year_2019.txt")
# many  birds were caught later in their life... add a status for when they were 0.5 or 1 year old!
d$age <- d$age_count
d[d$age>15, "age"] <- 15
db <- d[d$Status=="BrF",c("age","SeasonID")]
index <- unique(db$SeasonID)
#db$sum_perYear <- NUL
#for ( i in 1:length(index)){
# db[db$SeasonID=i,"sum_perYear"] <- ave(db[db$SeasonID=i,"age"], db[db$SeasonID=i,"age"], FUN= length)
#}
db$sum <- ave(db$age, db$age, FUN=length)
db <- unique(db[order(db$age),c("age","sum")])
#age0 <- data.frame(age=0.5, sum=0)
#db <- rbind(db, age0)
db$tot <- sum(db$sum)
db$norm <- db$sum/db$tot
db <- unique(db[order(db$age),])

dh <- d[d$Status=="H",c("age","SeasonID")]
index <- unique(dh$SeasonID)
dh$sum <- ave(dh$age, dh$age, FUN=length)
dh <- unique(dh[order(dh$age),c("age","sum")])
dh$tot <- sum(dh$sum)
dh$norm <- dh$sum/dh$tot
dh <- unique(dh[order(dh$age),])

dab <- d[d$Status=="AB",c("age","SeasonID")]
index <- unique(dab$SeasonID)
dab$sum <- ave(dab$age, dab$age, FUN=length)
dab <- unique(dab[order(dab$age),c("age","sum")])
age0 <- data.frame(age=i.par$meshpts[(i.par$meshpts %in% dab$age== FALSE)])
age0$sum <- rep(0,nrow(age0))
dab <- rbind(dab, age0)
dab$tot <- sum(dab$sum)
dab$norm <- dab$sum/dab$tot
dab <- unique(dab[order(dab$age),])


dnsa <- d[d$Status=="NSA",c("age","SeasonID")]
index <- unique(dnsa$SeasonID)
dnsa$sum <- ave(dnsa$age, dnsa$age, FUN=length)
dnsa <- unique(dnsa[order(dnsa$age),c("age","sum")])
age0 <- data.frame(age=i.par$meshpts[(i.par$meshpts %in% dab$age== FALSE)])
age0$sum <- rep(0,nrow(age0))
dnsa <- rbind(dnsa, age0)
dnsa$tot <- sum(dnsa$sum)
dnsa$norm <- dnsa$sum/dnsa$tot
dnsa <- unique(dnsa[order(dnsa$age),])


dsub <- d[d$Status!="BrF",c("age","SeasonID")]
index <- unique(dsub$SeasonID)
dsub$sum <- ave(dsub$age, dsub$age, FUN=length)
dsub <- unique(dsub[order(dsub$age),c("age","sum")])
age0 <- data.frame(age=i.par$meshpts[(i.par$meshpts %in% dab$age== FALSE)])
age0$sum <- rep(0,nrow(age0))
dsub <- rbind(dsub, age0)
dsub$tot <- sum(dsub$sum)
dsub$norm <- dsub$sum/dsub$tot
dsub <- unique(dsub[order(dsub$age),])


#altogether
source("population_model/IBM/plot_IBM_agesAll_lines_by_rep.R")
source("population_model/IBM/plot_agesAll_withShades.R")
source("population_model/IBM/plot_IBM_agesAll_lines.R")
###################################################################################
###################################################################################







print("other plots done")

graphics.off()




sessionInfo()
